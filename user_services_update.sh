#!/usr/bin/env bash
#title		    :bash.sh
#description	:
#author		    :Valeriu Stinca
#email		    :ts@strategic.zone
#date		    :20180206
#version	    :0.1
#notes		    :
#===================
systemctl --user daemon-reload
systemctl --user disable coke-remote.service
systemctl --user stop coke-remote.service
systemctl --user enable coke-carrefour-reboot.service
systemctl --user start coke-carrefour-reboot.service
echo "stop and set key clearCache to false"
