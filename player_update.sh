#!/usr/bin/env bash
#title		    :bash.sh
#description	:
#author		    :Valeriu Stinca
#email		    :ts@strategic.zone
#date		    :20180206
#version	    :0.1
#notes		    :
#===================

gpg_password="${1}"
base="$(mktemp -d)"
vpn_config="$(hostname).xlan.io.ovpn"

# Upload archives
wget -O ${base}/player_update.tar.xz.gpg https://gitlab.com/strategic.zone/players_update/raw/master/player_update.tar.xz.gpg
wget -O ${base}/client.ovpn.gpg "https://gitlab.com/strategic.zone/players_update/raw/master/vpn_configs/$(hostname)-vpn.xlan.io.ovpn.gpg"

# decrypt and untar archives
gpg -d --batch --passphrase "${gpg_password}" ${base}/player_update.tar.xz.gpg > ${base}/player_update.tar.xz
# rm ${base}/player_update.tar.xz.gpg
tar xvf ${base}/player_update.tar.xz -C /
# rm ${base}/player_update.tar.xz 
gpg -d --batch --passphrase "${gpg_password}" ${base}/client.ovpn.gpg > /opt/player/conf/client.ovpn
# rm ${base}/client.ovpn.gpg
echo "${base}"


# Install VPN services
ln -sf /opt/player/conf/client.ovpn /etc/openvpn/client/client.conf
systemctl daemon-reload
systemctl enable openvpn-client@client.service
# after reboot
# systemctl start openvpn-client@client.service

#Firewall
echo "restart firewall"
systemctl restart nftables

# disable old services
# disable ZT-info.timer pub-mqtt.timer
echo "disable ZT-info.timer pub-mqtt.timer"
systemctl disable ZT-info.timer pub-mqtt.timer openvpn-client@client-vpn.service dns-update@tun0.service dns-update@tun0.timer 

# delete ZT-info.timer pub-mqtt.timer
echo "delete ZT-info.timer pub-mqtt.timer"
rm /etc/systemd/system/ZT-info.* /etc/systemd/system/pub-mqtt.*

# delete old dns.info
rm /opt/player/conf/dns.info

rm /etc/openvpn/client/client-vpn.conf

# add MQTT ZT and keepalive
# publish initial MQTT message:
mosquitto_pub -h "mqtt.xlan.io" -t "players/$(hostname)/services/ZT/state" -u player -P ahxu2yaLagaigei -m 1 -r

echo "enable and start services"
systemctl enable mqtt_ZT.timer
systemctl enable mqtt_keepalive.timer
systemctl start mqtt_keepalive.service
systemctl start mqtt_ZT.service

systemctl enable dns-update@tun0.timer

# modify vnstat for monitoring tun0
vnstat --add -i tun0
vnstat -u -i tun0

sed -i "/^Interface*/c\\Interface\ \"tun0\"" /etc/vnstat.conf

# Add coke-reboot
echo "change user for apps"
chown -R player:player /opt/player/app

echo "done! please log in user player account and execute: sh <(curl --silent https://gitlab.com/strategic.zone/players_update/raw/master/user_services_update.sh)"
# echo "add moons to ZT"
# zerotier-cli orbit bd9d89a58b bd9d89a58b
# zerotier-cli orbit 78fa5d0464 78fa5d0464

echo " !!! check vpn and disable ZT !!!"
echo "systemctl disable zerotier-one.service"
echo "delete root.sfs and remake squashfs "
echo "change boot to live"
